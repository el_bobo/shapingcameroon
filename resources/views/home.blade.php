@extends('welcome')
@section('content')
    <!-- ======= Hero Section ======= -->
    <section id="hero" class="hero d-flex align-items-center"
        style="background-image: url({{ asset('img/home/header-1.jpg') }})">
        <div class="container">
            <div class="row gy-4 d-flex justify-content-between">
                <div class="col-lg-6 order-1 order-lg-1 d-flex flex-column justify-content-center">
                    <h2 data-aos="fade-up"><span class="text-danger">SHAPING</span> CAMEROON</h2>
                </div>

                <div class="col-lg-12 order-3 order-lg-1 d-flex flex-column justify-content-center">
                    <div class="play-video">
                        <div class="circle-play">
                            <i class="fa-solid fa-circle-play"></i>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 order-3 text-end ticket-case">
                    <h4 class="text-danger">22 JUIN 2024</h4>
                    <h5>Palais des Congrès, Yaoundé - Cameroun</h5>
                    <a class="btn-buy-outline" href="">Reserve your ticket</a>
                </div>

            </div>
        </div>
    </section><!-- End Hero Section -->

    <section class="count-down-area top-padding" style="background-color: #010f2f">
        <div class="container">
            <div class="row justify-content-between ">
                <div class="col-xl-4 col-lg-4 col-md-6">

                    <div class="section-tittle mb-40">
                        <h2>About <br> SHAPING CAMEROON</h2>
                        <a href="#" class="btn_1 mt-20">Confirm Your Seat<i class="ti-arrow-right"></i></a>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-7 col-md-12">

                    <div class="section-tittle mb-40">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, vel omnis repellendus
                            mollitia, explicabo, maiores quisquam numquam quia reiciendis sit, accusantium atque ex animi
                            perspiciatis ab odit earum assumenda aliquid.</p>
                        <p>Dolor sit amet, consectetur adipisicing elit. Commodi, vel omnis repellendus mollitia, explicabo,
                            maiores quisquam numquam quia reiciendis sit, accusantium atque ex animi perspiciatis ab odit
                            earum assumenda aliquid.</p>
                    </div>

                    <div class="count-down-wrapper">
                        <div class="row justify-content-between">
                            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4">

                                <div class="single mb-30">
                                    <div class="single-counter">
                                        <p>0</p>
                                        <span class="counter ">3</span>
                                    </div>
                                    <div class="pera-count">
                                        <p>Day Event</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4">
                                <div class="single mb-30">

                                    <div class="single-counter">
                                        <span class="counter ">10</span>
                                        <p class="">+</p>
                                    </div>
                                    <div class="pera-count">
                                        <p>Day Event</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-4 col-md-4 col-sm-4">
                                <div class="single mb-30">

                                    <div class="single-counter">
                                        <span class="counter ">23</span>
                                        <p class="">+</p>
                                    </div>
                                    <div class="pera-count">
                                        <p>Sponsors</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="team-area section-padding" style="background-color: #010f2f">
        <div class="container">
            <div class="row justify-content-center mb-3">
                <div class="cl-xl-12">
                    <div
                        class="section-tittle text-center mb-40 d-flex justify-content-between flex-wrap align-items-center">
                        <h2>Speakers</h2>
                        <a href="#" class="btn_01">View All Speakers<i class="ti-arrow-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="container overflow-hidden">
                        <div class="row gy-4 gy-lg-0 gx-xxl-5">
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="card border-0 border-bottom border-primary shadow-sm overflow-hidden">
                                    <div class="card-body p-0">
                                        <figure class="m-0 p-0">
                                            <img class="img-fluid" loading="lazy"
                                                src="https://creapills.com/wp-content/uploads/2023/01/pays-portraits-personnes-ia-47.jpg"
                                                alt="Flora Nyra">
                                            <figcaption class="m-0 p-4">
                                                <h4 class="mb-1">Flora Nyra</h4>
                                                <p class="text-secondary mb-0">Product Manager</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="card border-0 border-bottom border-primary shadow-sm overflow-hidden">
                                    <div class="card-body p-0">
                                        <figure class="m-0 p-0">
                                            <img class="img-fluid" loading="lazy"
                                                src="https://creapills.com/wp-content/uploads/2023/01/pays-portraits-personnes-ia-48.jpg"
                                                alt="Evander Mac">
                                            <figcaption class="m-0 p-4">
                                                <h4 class="mb-1">Evander Mac</h4>
                                                <p class="text-secondary mb-0">Art Director</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="card border-0 border-bottom border-primary shadow-sm overflow-hidden">
                                    <div class="card-body p-0">
                                        <figure class="m-0 p-0">
                                            <img class="img-fluid" loading="lazy"
                                                src="https://creapills.com/wp-content/uploads/2023/01/pays-portraits-personnes-ia-49.jpg"
                                                alt="Taytum Elia">
                                            <figcaption class="m-0 p-4">
                                                <h4 class="mb-1">Taytum Elia</h4>
                                                <p class="text-secondary mb-0">Investment Planner</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3">
                                <div class="card border-0 border-bottom border-primary shadow-sm overflow-hidden">
                                    <div class="card-body p-0">
                                        <figure class="m-0 p-0">
                                            <img class="img-fluid" loading="lazy"
                                                src="https://creapills.com/wp-content/uploads/2023/01/pays-portraits-personnes-ia-46.jpg"
                                                alt="Wylder Elio">
                                            <figcaption class="m-0 p-4">
                                                <h4 class="mb-1">Wylder Elio</h4>
                                                <p class="text-secondary mb-0">Financial Analyst</p>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="about-area" style="background-color: #010f2f">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xxl-7 col-xl-7 col-lg-6 col-md-8">

                    <div class="about-img">
                        <img src="{{ asset('img/home/header-2.jpg') }}" alt="">
                    </div>
                </div>
                <div class="offset-xxl-1 col-xxl-4 col-xl-5 col-lg-6 col-md-9">
                    <div class="about-caption">

                        <div class="section-tittle mb-25">
                            <h2>A WEEK OF <span>SHAPING <br>IN CAMEROON</span></h2>
                            <p>Dolor sit amet, consectetur adipisicing elit. Commodi, vel omnis repellendus mollitia,
                                explicabo, maiores quisquam numquam quia reiciendis sit, accusantium atque ex animi
                                perspiciatis ab odit earum assumenda aliquid santium atque ex animi.</p>
                            <p>Dolor sit amet, consectetur adipisicing elit. Commodi, vel omnis repellendus mollitia,
                                explicabo, maiores quisquam numquam quia reiciendis sit, accusantium atque ex animi
                                perspiciatis ab odit earum assumenda aliquid santium atque ex animi.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="shedule-area section-padding" style="background-color: #010f2f">
        <div class="container">
            <div class="row">
                <div class="col-xl-5 col-lg-6 col-md-6">

                    <div class="section-tittle  mb-30">
                        <h2>Schedule</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-11">
                    <div class="nav-button">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-link active" id="nav-one-tab" data-bs-toggle="tab" href="#nav-one"
                                    role="tab" aria-controls="nav-one" aria-selected="true">17th Aug</a>
                                <a class="nav-link" id="nav-two-tab" data-bs-toggle="tab" href="#nav-two"
                                    role="tab" aria-controls="nav-two" aria-selected="false">18th Aug</a>
                                <a class="nav-link" id="nav-three-tab" data-bs-toggle="tab" href="#nav-three"
                                    role="tab" aria-controls="nav-three" aria-selected="false">19th Aug</a>
                            </div>
                        </nav>

                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-one" role="tabpanel" aria-labelledby="nav-one-tab">

                    <div class="row justify-content-center">
                        <div class="col-lg-11">
                            <div class="shedule-wrapper">

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Registration And Breakfast</h4>
                                                </a>
                                                <p>- Mal Practice</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>8:30 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Opening Remarks And Keynote</h4>
                                                </a>
                                                <p>- Aaron Ottix</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>10:00 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Web Accessible Designs</h4>
                                                </a>
                                                <p>- Frank Senbeans</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>11:30 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Building Communities</h4>
                                                </a>
                                                <p>- Ken Tucky</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>12:30 am</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-two" role="tabpanel" aria-labelledby="nav-two-tab">

                    <div class="row justify-content-center">
                        <div class="col-lg-11">
                            <div class="shedule-wrapper">

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Registration And Breakfast</h4>
                                                </a>
                                                <p>- Mal Practice</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>8:30 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Opening Remarks And Keynote</h4>
                                                </a>
                                                <p>- Aaron Ottix</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>10:00 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Web Accessible Designs</h4>
                                                </a>
                                                <p>- Frank Senbeans</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>11:30 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Building Communities</h4>
                                                </a>
                                                <p>- Ken Tucky</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>12:30 am</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-three" role="tabpanel" aria-labelledby="nav-three-tab">

                    <div class="row justify-content-center">
                        <div class="col-lg-11">
                            <div class="shedule-wrapper">

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Registration And Breakfast</h4>
                                                </a>
                                                <p>- Mal Practice</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>8:30 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Opening Remarks And Keynote</h4>
                                                </a>
                                                <p>- Aaron Ottix</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>10:00 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Web Accessible Designs</h4>
                                                </a>
                                                <p>- Frank Senbeans</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>11:30 am</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="single-items">
                                    <div class="shedule-items">
                                        <div class="shedule-left">
                                            <div class="job-tittle">
                                                <a href="job_details.html">
                                                    <h4>Building Communities</h4>
                                                </a>
                                                <p>- Ken Tucky</p>
                                            </div>
                                        </div>
                                        <div class="price">
                                            <span>12:30 am</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home-blog bottom-padding" style="background-color: #010f2f">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-blogs mb-30">
                        <div class="blog-img blog-img2">
                            <a href="#"><img src="https://preview.colorlib.com/theme/eventotemplate/assets/img/gallery/blog1.jpg" alt=""></a>
                        </div>
                        <div class="blogs-cap">
                            <h5><a href="#">WHEN CORNERS COLLIDE: DECODING THE CONTRASTING AESTHETICS</a></h5>
                            <p>- Justin Case</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-blogs mb-30">
                        <div class="blog-img blog-img2">
                            <a href="#"><img src="https://preview.colorlib.com/theme/eventotemplate/assets/img/gallery/blog2.jpg" alt=""></a>
                        </div>
                        <div class="blogs-cap">
                            <h5><a href="#">WHEN CORNERS COLLIDE: DECODING THE CONTRASTING AESTHETICS</a></h5>
                            <p>- Justin Case</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="single-blogs mb-30">
                        <div class="blog-img blog-img2">
                            <a href="#"><img src="https://preview.colorlib.com/theme/eventotemplate/assets/img/gallery/blog3.jpg" alt=""></a>
                        </div>
                        <div class="blogs-cap">
                            <h5><a href="#">WHEN CORNERS COLLIDE: DECODING THE CONTRASTING AESTHETICS</a></h5>
                            <p>- Justin Case</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
